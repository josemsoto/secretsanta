import { Component, OnInit, Input } from '@angular/core';
import { SideMenuData } from '../models/SideMenuData';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  @Input() menuData:SideMenuData;

  constructor() {
  }

  ngOnInit() {
  }

}
