import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CanActivateAuthGuard }  from './guards/can-activate-auth.guard';
import { ParticipantsComponent } from './participants/participants.component';
import { ProfileAboutComponent } from './profile-about/profile-about.component';
import { ProfileWishListComponent } from './profile-wish-list/profile-wish-list.component';

const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'profile',
    canActivate:[ CanActivateAuthGuard ],
    component: UserProfileComponent,
    children: [ { path:'', redirectTo:'/profile/about', pathMatch: 'full' },
                { path:'about', component:ProfileAboutComponent },
                { path:'wish-list', component: ProfileWishListComponent }
              ]
  },
  { path: '**',   component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
