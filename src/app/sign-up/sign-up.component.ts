import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { SignupUser } from '../models/SignupUser'
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  first_name: string;
  last_name: string;
  email: string;
  password: string;
  confirm: string;
  errorMessage:string;
  constructor(public authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  signUp(){

    this.errorMessage = null;

    if (this.password != this.confirm) {
      this.errorMessage = "Passwords do not match.";
      return;
    }

    var newUser:SignupUser = {
      email : this.email,
      first_name: this.first_name,
      last_name : this.last_name,
      password : this.password
    };

    this.authService
        .signup(newUser, '/home')
        .then(() => {
          this.router.navigate(['/']);
        });
  }

}
