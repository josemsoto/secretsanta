import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: []
})
export class HomeComponent implements OnInit {

  email:string;
  password: string;
  titleText:string = "Welcome to Soto's Secret Santa";
  showContent:boolean = false;
  constructor(public authService: AuthService, public dataService:DataService) {
    this.authService.user.subscribe(user => {
      this.showContent = (user!=null);
    })
  }

  ngOnInit() { }

}
