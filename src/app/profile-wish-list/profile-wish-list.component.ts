import { Component, OnInit } from '@angular/core';
import { LinkPreviewService } from '../services/link-preview.service';
import { LinkPreviewResponse } from '../models/LinkPreviewResponse';
import { WishListItem } from '../models/WishListItem';
import { WishListService } from '../services/wish-list.service';

@Component({
  selector: 'app-profile-wish-list',
  templateUrl: './profile-wish-list.component.html',
  styleUrls: ['./profile-wish-list.component.css'],
  providers: [ LinkPreviewService ]
})
export class ProfileWishListComponent implements OnInit {

  isEditing = false;
  addEnabled = true;
  searchText = "";

  constructor(private lps:LinkPreviewService, public wls:WishListService) { }

  ngOnInit() {
    
  }

  searchLink() {
    this.addEnabled = false;
    this.lps.getLinkPreview(this.searchText).then((result:LinkPreviewResponse) => {
      if (result) {
        // Save result as wish list item on firebase store.
        this.wls.saveWishListItem(result as LinkPreviewResponse);
        this.searchText = "";
        this.addEnabled = true;
      }
    });
  }
}
