import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileWishListComponent } from './profile-wish-list.component';

describe('ProfileWishListComponent', () => {
  let component: ProfileWishListComponent;
  let fixture: ComponentFixture<ProfileWishListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileWishListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileWishListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
