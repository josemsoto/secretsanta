import { Component, OnInit, Input } from '@angular/core';
import { WishListItem } from '../models/WishListItem';
import { WishListService } from '../services/wish-list.service';
import { MatDialog } from '@angular/material';
import { ImageLinkDialogComponent } from '../image-link-dialog/image-link-dialog.component';
import { DeleteConfirmDialogComponent } from '../delete-confirm-dialog/delete-confirm-dialog.component';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-wish-list-item',
  templateUrl: './wish-list-item.component.html',
  styleUrls: ['./wish-list-item.component.css'],
})
export class WishListItemComponent implements OnInit {

  @Input() data:WishListItem;

  _canEdit
  @Input() set canEdit(value:boolean) {
    this._canEdit = value;
    if (!value) this.isEditing = false;
  }
  get canEdit(): boolean {
    return this._canEdit;
  }


  isEditing:boolean;
  titleFormControl = new FormControl('', [
      Validators.required
    ]);

  constructor(private wls:WishListService, public dialog: MatDialog) { }

  ngOnInit() {
  }

  deleteItem() {
    if (!this.data) return;
    // this.wls.deleteWishListItem(this.data.id);
    let dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width:'400px',
      data: {message: "Are you sure you want to delete:",
             submessage: this.data.title}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.wls.deleteWishListItem(this.data.id);
      }
    });
  }

  updateItem() {
    if (!this.data) return;

    if (this.titleFormControl.hasError('required')) return;

    this.wls.updateWishListItem(this.data);
    this.isEditing = false;
  }

  setImage() {
    let dialogRef = this.dialog.open(ImageLinkDialogComponent, {
      width:'400px',
      data: {link: this.data.image}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.data.image = result;
      }
    });
  }
}
