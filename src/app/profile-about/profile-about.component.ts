import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormControl, Validators } from '@angular/forms';
import { ImageLinkDialogComponent } from '../image-link-dialog/image-link-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-profile-about',
  templateUrl: './profile-about.component.html',
  styleUrls: ['./profile-about.component.css']
})
export class ProfileAboutComponent implements OnInit {

  isEditing:boolean;
  about:string;
  fnameFormControl = new FormControl('', [
       Validators.required
    ]);
  lnameFormControl = new FormControl('', [
      Validators.required
    ]);

  constructor(public dataService:DataService, public dialog: MatDialog) {
    this.isEditing = false;
  }

  ngOnInit() {
  }

  editSave() {
    this.isEditing = !this.isEditing;
    if (!this.isEditing) {
      this.saveAbout();
    }
  }

  saveAbout() {
    if (this.fnameFormControl.hasError('required')) return;
    if (this.lnameFormControl.hasError('required')) return;
    this.isEditing=false;
    this.dataService.updateCurrentUser();
  }

  setImage() {
    let dialogRef = this.dialog.open(ImageLinkDialogComponent, {
      width:'400px',
      data: {link: this.dataService.currentUserObject.image}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        console.log(result);
        this.dataService.currentUserObject.image = result;
      }
    });
  }
}
