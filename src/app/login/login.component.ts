import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  errorMessage: string;

  constructor(public authService:AuthService) { }

  ngOnInit() {
  }

  login() {
    this.errorMessage = "";
    if (this.email == null || this.email.trim() == "") { this.errorMessage = "Email is empty."; return;}
    if (this.password == null || this.password.trim() == "") { this.errorMessage = "Password is empty."; return;}

    this.authService.login(this.email, this.password);
    this.email = this.password = '';
  }

  logout() {
    this.authService.logout();
  }

  loginWithGoogle() {
    this.errorMessage = "";
    this.authService.loginWithGoogle();
    this.email = this.password = '';
  }

}
