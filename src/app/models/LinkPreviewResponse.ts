export class LinkPreviewResponse {
  title?:string;
  description?:string;
  image?:string;
  url?:string;
}
