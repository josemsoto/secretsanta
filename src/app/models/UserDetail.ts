export interface UserDetail {
  uid?: string;
  first_name?: string;
  last_name?: string;
  image?:string;
  about?:string;
  group_id?:number;
}
