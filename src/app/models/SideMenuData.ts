import { SideMenuLink } from './SideMenuLink';

export class SideMenuData {
  title:string;
  links:SideMenuLink[];
}
