export interface SideMenuLink {
  name:string;
  link:string;
}
