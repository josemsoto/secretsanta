export class LoginResponse {
  uid:string;
  displayName:string;
  email:string;
  emailVerified:boolean;
  isAnonymous:boolean;
  phoneNumber:string;
  photoURL:string;
  refreshToken:string;
}
