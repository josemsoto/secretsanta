import { LinkPreviewResponse } from '../models/LinkPreviewResponse';

export class WishListItem {
  id:string;
  user:string;
  title?:string;
  description?:string;
  image?:string;
  url?:string;
  created_date:Date
}
