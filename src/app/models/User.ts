import { UserDetail } from './UserDetail';

export class User implements UserDetail {
  id:string;
  uid?: string;
  first_name?: string;
  last_name?: string;
  image?:string;
  about?:string;
  group_id?:number;

  constructor(id:string, details:UserDetail) {
    this.id = id;
    this.first_name = details.first_name;
    this.last_name = details.last_name;
    this.uid = details.uid;
    this.image = details.image;
    this.about = details.about;
    this.group_id = details.group_id
  }
}
