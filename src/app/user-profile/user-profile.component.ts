import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { User } from '../models/User';
import { SideMenuData } from '../models/SideMenuData';
import { SideMenuLink } from '../models/SideMenuLink';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  menuData:SideMenuData

  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.menuData = {
      title : "Settings",
      links : [
        {name: "About", link: "/profile/about"} as SideMenuLink,
        {name: "Wish List", link: "/profile/wish-list"} as SideMenuLink
      ]
    };
  }

}
