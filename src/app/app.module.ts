import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { WishListService } from './services/wish-list.service';
import { CanActivateAuthGuard } from './guards/can-activate-auth.guard';

import { BackgroundImageDirective } from './directives/background-image.directive';
import { DisableControlDirective } from './directives/disable-control.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserWindowComponent } from './user-window/user-window.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AssignedUserComponent } from './assigned-user/assigned-user.component';
import { ParticipantsComponent } from './participants/participants.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { ProfileAboutComponent } from './profile-about/profile-about.component';
import { ProfileWishListComponent } from './profile-wish-list/profile-wish-list.component';
import { WishListItemComponent } from './wish-list-item/wish-list-item.component';
import { ImageLinkDialogComponent } from './image-link-dialog/image-link-dialog.component';
import { DeleteConfirmDialogComponent } from './delete-confirm-dialog/delete-confirm-dialog.component';


export const firebaseConfig = {
    apiKey: "AIzaSyAvpaVDypRsiTaxcc3Ukmcckhg48rvFjdM",
    authDomain: "secret-santa-255ee.firebaseapp.com",
    databaseURL: "https://secret-santa-255ee.firebaseio.com",
    storageBucket: "secret-santa-255ee.appspot.com",
    projectId: "secret-santa-255ee",
    messagingSenderId: "858136383622"
  };

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    UserWindowComponent,
    LoginComponent,
    SignUpComponent,
    AssignedUserComponent,
    ParticipantsComponent,
    UserProfileComponent,
    SideMenuComponent,
    ProfileAboutComponent,
    ProfileWishListComponent,
    WishListItemComponent,
    ImageLinkDialogComponent,
    BackgroundImageDirective,
    DisableControlDirective,
    DeleteConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    AuthService,
    DataService,
    WishListService,
    CanActivateAuthGuard
  ],
  entryComponents: [
    ImageLinkDialogComponent,
    DeleteConfirmDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
