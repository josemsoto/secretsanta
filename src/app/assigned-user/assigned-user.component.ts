import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/User';
import { WishListService } from '../services/wish-list.service';
import { WishListItem } from '../models/WishListItem';

@Component({
  selector: 'app-assigned-user',
  templateUrl: './assigned-user.component.html',
  styleUrls: ['./assigned-user.component.css']
})
export class AssignedUserComponent implements OnInit {

  _assignedUser: User;
  get assignedUser(): User {
    return this._assignedUser;
  }

  @Input('assignedUser')
  set assignedUser(value: User) {
    this._assignedUser = value;
    if (value)
      this.wishList = this.wls.getUserList(this.assignedUser.id);
    else
      this.wishList = null;
  }

  wishList:Observable<WishListItem[]>;

  constructor(public wls:WishListService) {

  }

  ngOnInit() {
  }

}
