import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

import { LoginResponse } from '../models/LoginResponse';
import { SignupUser } from '../models/SignupUser'
import { User } from '../models/User';

@Injectable()

export class AuthService {

  public errorMessage:string = null;
  user: Observable<firebase.User>;
  userId: Observable<string>;

  constructor(private afAuth: AngularFireAuth, private afs:AngularFirestore, private router:Router) {
    this.user = afAuth.authState;

    // Get current user logged into auth service.
    this.userId = this.user.map(x => {
      if (x == null) return null;
      var email = x.email;
      if (email == null && x.providerData && x.providerData.length > 0)
        email = x.providerData[0].email;

      // this.userId = new Observable<string>(x => x.next(email) );
      return email;
    });
  }

  isLoggedIn(){
    return this.user.map(x => {
        return x!=null;
    });
  }

  signup(newUser:SignupUser, reroutePath:string) {
    this.errorMessage = null;

    // Get email and validate if user is authorized to participate.
    // This prevents anyone that is not pre-registered, to sign up.
    return this.validateUser(newUser.email)
        .then(() => {
           return this.afAuth
               .auth
               .createUserWithEmailAndPassword(newUser.email, newUser.password)
               .then((response:LoginResponse) => {
                  return this.updateUserDetails(response.uid, newUser)
                      .then(() => {
                        return this.login(newUser.email, newUser.password);
                      })
                      .catch(error => {
                        console.log("User update error: " + error.message);
                        this.errorMessage = error.message;
                      });
               })
               .catch(error => {
                 console.log("User creation error: " + error.message);
                 this.errorMessage = error.message;
               });
        })
        .catch(error => {
          console.log("User validation error: " + error.message);
          this.errorMessage = error.message;
        });



  }

  loginWithGoogle() {
    this.errorMessage = null;
    var provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/userinfo.email');
    return this.afAuth
               .auth
               .signInWithPopup(provider)
               .then(result => {

                 if (result.user == null || result.user.providerData == null || result.user.providerData.length == 0) {
                   console.log("Google login error. Contact administrator.");
                   return;
                 }

                 // Get email and validate if user is authorized to participate.
                 // This prevents anyone that is not pre-registered, to sign up.
                 var email = result.user.providerData[0].email;
                 var uid = result.user.uid;
                 var names = result.user.providerData[0].displayName.split(" ");
                 var first_name = names.length > 0 ? names[0]:"";
                 var last_name = names.length > 1 ? names[1]:"";

                 var user:SignupUser = {email : email, first_name : first_name, last_name : last_name} as SignupUser;

                 return this.validateUser(email).then(() => {
                   return this.updateUserDetails(uid, user)
                   .then(() => {
                     this.router.navigate(['/']);
                   })
                   .catch(error => {
                     console.log("User update error: " + error.message);
                     this.errorMessage = error.message;
                   });
                 })
                 .catch(error => {
                   console.log("User validation error: " + error.message);
                   this.errorMessage = error.message;
                 });
               })
               .catch(error => {
                 console.log("Google Login error: " + error.message);
                 this.errorMessage = error.message;
               });
  }

  login(email:string, password:string) {
    this.errorMessage = null;
    return this.afAuth
               .auth
               .signInWithEmailAndPassword(email, password)
               .then(() => {
                 this.router.navigate(['/']);
               })
               .catch(error => {
                 console.log("Login error: " + error.message);
                 this.errorMessage = error.message;
               });
  }

  logout() {
    this.errorMessage = null;
    return this.afAuth
               .auth
               .signOut()
               .then(() => {
                 this.router.navigate(['/']);
               });
  }

  private updateUserDetails(uid:string, newUser:SignupUser) {
    var userRef = this.afs.collection<User>('users').doc(newUser.email);
    return userRef.update({
      uid: uid,
      first_name: newUser.first_name,
      last_name: newUser.last_name
    });
  }

  private validateUser(email:string) {
    this.errorMessage = null;
    return this.afs.doc<User>("users/" + email).ref.get().then((result)=>{
      if (result.exists == false) throw new Error("User is not authorized.");
    });

  }

}
