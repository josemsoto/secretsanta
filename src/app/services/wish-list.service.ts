import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { AuthService } from './auth.service';
import { WishListItem } from '../models/WishListItem';
import { LinkPreviewResponse } from '../models/LinkPreviewResponse';

@Injectable()
export class WishListService {

  userLinks:Observable<WishListItem[]>;
  errorMessage:string;
  currentUser:string;

  constructor(private afs:AngularFirestore, private authService:AuthService) {
    // Subscribe to user specific wish list links
    this.authService.userId.subscribe(user => {
      if (!user) {
        this.userLinks = null;
        return;
      }
      this.currentUser = user;
      this.userLinks = this.getUserList(user);
    });
  }

  getUserList(user:string){
    return this.afs.collection('wish-list-items', ref => ref.where('user','==',user).orderBy('created_date','desc'))
            .snapshotChanges()
            .map(actions => {
              // debugger;
              return actions.map(data => {
                var id = data.payload.doc.id;
                var item = data.payload.doc.data();
                return {
                  id: id,
                  user: item.user,
                  title: item.title,
                  description: item.description,
                  image: item.image,
                  url: item.url
                } as WishListItem
              });
            });
  }

  deleteWishListItem(id:string) {
    return this.afs.doc('wish-list-items/'+id).delete();
  }

  saveWishListItem(item:LinkPreviewResponse) {
    this.errorMessage = "";
    if (!item || !this.currentUser) return;
    return this.afs.collection('wish-list-items')
                   .add({
                     user: this.currentUser,
                     title: item.title,
                     description: item.description,
                     image: item.image,
                     url: item.url,
                     created_date: new Date()
                   })
                   .catch(error => {
                     console.log("Error saving wishlist item: " + error.message);
                     this.errorMessage = error.message;
                   });
  }

  updateWishListItem (item:WishListItem) {
    this.errorMessage = "";
    if (!item || !this.currentUser) return;

    var itemRef = this.afs.collection('wish-list-items').doc(item.id);
    return itemRef.update({
      title: item.title,
      description: item.description,
      image: item.image
    })
  }

}
