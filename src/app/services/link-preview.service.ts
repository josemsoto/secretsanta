import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { LinkPreviewResponse } from '../models/LinkPreviewResponse';
import 'rxjs/add/operator/timeout';
import { TimeoutError } from 'rxjs/util/TimeoutError';


@Injectable()
export class LinkPreviewService {

  errorMessage:string;

  constructor(private http: HttpClient) { }

  getLinkPreview(link:string){
    let params:HttpParams = new HttpParams()
    params = params.set('key','5a0dffaa71577204fdcae117b31aa8d74aad10e7e37ce');
    params = params.set('q',link);

    return this.http.get<LinkPreviewResponse>('https://api.linkpreview.net',{
      params: params
    })
    .timeout(5000)
    .toPromise()
    .then((result:LinkPreviewResponse) => {
      return result;
    })
    .catch(error => {
      if (error as TimeoutError) {
        return {
          description: "",
          image: "",
          title: link,
          url: link,
        };
      }else{
        console.log("Error obtaining link preview: " + error.message);
        this.errorMessage = error.message;
      }
    });
  }

}
