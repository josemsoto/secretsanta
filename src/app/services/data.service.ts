import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';

import { User } from '../models/User';
import { AssignedUser } from '../models/AssignedUser';

@Injectable()
export class DataService {

  users:User[];
  assignedUsersList:AssignedUser[];
  assignedUser:User;

  currentUser:string;
  currentUserObject:User;
  errorMessage:string;

  constructor(private afs:AngularFirestore, private authService:AuthService) {
    this.afs.collection('users')
            .snapshotChanges()
            .map(actions => {
              return actions.map(data => {
                var id = data.payload.doc.id;
                var user = data.payload.doc.data();
                return new User(id, user);
              });
            }).subscribe(x => {
              this.users = x as User[];
            });

    // Get current user logged into auth service.
    this.authService.userId.subscribe(user => {
      if (!user) {
        this.currentUser = null;
        this.currentUserObject = null;
        this.assignedUser = null;
        return;
      }

      this.currentUser = user;
      this.afs.collection('users')
              .doc(this.currentUser)
              .valueChanges()
              .subscribe(x => {
                this.currentUserObject = new User(this.currentUser, x);
              });

      this.getAssignedUser();
    });

    // Get array of assigned users.
    this.afs.collection('assigned_users')
            .valueChanges()
            .subscribe(x => {
      this.assignedUsersList = x as AssignedUser[];
    });

  }

  setAssignedUser(){
    if (this.assignedUser != null) return;
    var availableUsers = this.users.filter(user => {
      var hits = this.assignedUsersList.filter(assignedUser => {
        return assignedUser.user == user.id
      });
      return (hits.length==0 && user.id != this.currentUser && user.group_id != this.currentUserObject.group_id);
    });
    var randomPos = this.getRandomInt(1, availableUsers.length) - 1;
    this.createNewUserAssignment(availableUsers[randomPos]);
  }

  updateCurrentUser() {
    if (this.currentUserObject == null) return;

    var userRef = this.afs.collection('users').doc(this.currentUserObject.id);
    return userRef.update({
      uid: this.currentUserObject.uid,
      first_name: this.currentUserObject.first_name,
      last_name: this.currentUserObject.last_name,
      image: this.currentUserObject.image,
      about: this.currentUserObject.about,
      group_id: this.currentUserObject.group_id
    });
  }

  private getAssignedUser(){
    if (this.currentUser == null) return;
    this.assignedUser = null;
    this.afs.doc('assigned_users/' + this.currentUser)
            .valueChanges()
            .subscribe((x:AssignedUser) => {
              if (x == null || !this.users || this.users.length == 0) {
                this.assignedUser = null;
                return;
              }
              var assignedUser:User[] = this.users.filter(user => user.id == x.user);
              this.assignedUser = assignedUser.length == 1 ? assignedUser[0] as User : null;
            });
  }

  private createNewUserAssignment(assignedUser:User){
    if (this.currentUser == null) return;
    var assignedUserRef = this.afs.collection<AssignedUser>('assigned_users').doc(this.currentUser);
    assignedUserRef.set({
      user: assignedUser.id,
      date_assigned: new Date()
    })
    .catch(error => {
      console.log("Error assigning user: " + error.message);
      this.errorMessage = error.message;
    });
  }

  private getRandomInt(min:number, max:number) : number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
