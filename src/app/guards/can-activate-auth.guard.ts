import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';


@Injectable()
export class CanActivateAuthGuard implements CanActivate {

  constructor(private authService:AuthService, private router:Router) {}

  canActivate() {
    return this.authService.isLoggedIn().map(x => {
      if (!x)
        this.router.navigate(['/']);

      return x;
    });
  }
}
