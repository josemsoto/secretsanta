import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appBackgroundImage]'
})
export class BackgroundImageDirective {

  @Input('imageUrl') url: string;

  constructor(private el: ElementRef) { }

  ngOnChanges(changes) {
      this.updateBackground(changes.url.currentValue);
  }

  private updateBackground(url: string) {
    this.el.nativeElement.style.background = "url('" + url + "') no-repeat center center fixed";
    this.el.nativeElement.style.backgroundSize = "cover";
  }

}
