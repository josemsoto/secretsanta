import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-image-link-dialog',
  templateUrl: './image-link-dialog.component.html',
  styleUrls: ['./image-link-dialog.component.css']
})
export class ImageLinkDialogComponent implements OnInit {


  link:string;

  constructor(public dialogRef: MatDialogRef<ImageLinkDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                this.link = data.link;
              }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close(this.link);
  }

  close() {
    this.dialogRef.close(this.data.link);
  }
}
