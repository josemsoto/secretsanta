import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageLinkDialogComponent } from './image-link-dialog.component';

describe('ImageLinkDialogComponent', () => {
  let component: ImageLinkDialogComponent;
  let fixture: ComponentFixture<ImageLinkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageLinkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageLinkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
