// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();


// Setup firebase
var firebase = require("firebase");
var config = {
  apiKey: "AIzaSyAvpaVDypRsiTaxcc3Ukmcckhg48rvFjdM",
  authDomain: "secret-santa-255ee.firebaseapp.com",
  databaseURL: "https://secret-santa-255ee.firebaseio.com",
  storageBucket: "secret-santa-255ee.appspot.com",
};
firebase.initializeApp(config);

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



// // Point static path to dist
// app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
// var firebaseMiddleware = require('express-firebase-middleware');
// router.use('/api', firebaseMiddleware.auth);


// APIs
app.post('/login', function(req,res){

    var email = req.body.email;
    var pass = req.body.password;

    firebase.auth().signInWithEmailAndPassword(email, pass).then(function() {
      console.log(`Logged in: ${firebase.auth().currentUser.email}`);
      res.json({
          result: true
      });
    }).catch(function(error){
      var errorMessage = `Error Code: ${error.code}, Error Message: ${error.message}`;
      console.log(`Log in error for (${email}). ${errorMessage}`)
      res.json({
          result:false,
          message: `${errorMessage}`
      });
    });
});

app.post('/logoff', function(req,res){

    var user = firebase.auth().currentUser;
    if (user != null){
      console.log(`Logged off user: ${user.email}`)

      firebase.auth().signOut().then(function(){
        res.json({
            message: true
        });
      }).catch(function(error){
        var errorMessage = `Error Code: ${error.code}, Error Message: ${error.message}`;
        console.log(`Log off error for ${user.email}. ${error.message}`)
        res.json({
            result: false,
            message: `${error.message}`
        });
      });
    }
});


app.get('/getUsers', (req, res) => {
  var user = firebase.auth().currentUser;
  var result = `You are not logged in.`;
  if (user != null){
      result = `You are logged in as: ${user.email}`
  }
  res.json({
      message: `${result}`
  });

});


/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));
